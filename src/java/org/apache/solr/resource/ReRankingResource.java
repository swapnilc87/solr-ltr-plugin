package org.apache.solr.resource;

import org.apache.solr.common.SolrException;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.ltr.store.rest.ManagedPostProcessingStore;
import org.apache.solr.rest.ManagedResource;
import org.apache.solr.rest.ManagedResourceObserver;

import java.util.Map;

public class ReRankingResource implements ManagedResourceObserver {
    private ManagedPostProcessingStore postProcessingStore;

    private static ReRankingResource reRankingResource = new ReRankingResource();

    private ReRankingResource() {

    }

    public static ReRankingResource getInstance() {
        return reRankingResource;
    }

    public Map<String,Object> getReRankConfig() {
        return postProcessingStore.getConfigData();
    }

    @Override
    public void onManagedResourceInitialized(NamedList<?> args, ManagedResource res) throws SolrException {
        if (res instanceof ManagedPostProcessingStore) {
            synchronized (this) {
                postProcessingStore = (ManagedPostProcessingStore) res;
            }

        }
    }
}
