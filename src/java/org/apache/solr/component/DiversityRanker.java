package org.apache.solr.component;

import java.util.List;
import java.util.Map;

public interface DiversityRanker {
    List<CustomDocument> diversifyResults(List<CustomDocument> documents, Map<String,Object> config);
}
