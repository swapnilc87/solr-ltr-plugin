package org.apache.solr.component;

public interface CharlesConstants {
    String DYNAMIC_SLICING = "dynamicSlicing";
    String TRUE = "true";
    String ORIGINAL_START = "original_start";
    String START = "start";
    String ORIGINAL_ROWS = "original_rows";
    String ROWS = "rows";
    String RERANK_SIZE = "reRankDocs";
    Integer DEFAULT_RERANK_SIZE=192;
    Integer DEFAULT_START=0;
    Integer DEFAULT_ROWS=48;
    String DYNAMIC_SLICING_REQUIRED = "dynamicSlicingRequired";
    String RESPONSE = "response";
    String FIELD_LIST_PARAM = "fl";
    String COMMA = ",";


}
