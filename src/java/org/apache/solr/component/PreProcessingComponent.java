package org.apache.solr.component;

import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.handler.component.SearchComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class PreProcessingComponent extends SearchComponent implements CharlesConstants{

    private static final Logger LOG = LoggerFactory.getLogger(PreProcessingComponent.class);

    @Override
    public void prepare(ResponseBuilder rb) throws IOException {
        SolrParams params = rb.req.getParams();
        ModifiableSolrParams modifiableSolrParams = new ModifiableSolrParams(params);
        rb.req.setParams(modifiableSolrParams);
        if(modifiableSolrParams.get(DYNAMIC_SLICING)!=null && TRUE.equalsIgnoreCase(modifiableSolrParams.get(DYNAMIC_SLICING))) {
            Integer originalStart = modifiableSolrParams.getInt(START,DEFAULT_START);
            Integer originalRows = modifiableSolrParams.getInt(ROWS,DEFAULT_ROWS);
            Integer reRankSize = modifiableSolrParams.getInt(RERANK_SIZE,DEFAULT_RERANK_SIZE);

            if(LOG.isDebugEnabled()) {
                LOG.debug("Original Rows :"+originalRows);
                LOG.debug("Original Start :"+originalStart);
            }
            Integer rowsToFetch;
            if(originalStart < reRankSize && originalStart >=0) {
                if((originalStart+originalRows)>reRankSize) {
                    //we have unusual rows param passed which is greater than normal page size causing overflow
                    rowsToFetch=originalStart+originalRows;
                }else {
                    //in all other cases we take the entire rerank docs and slice it in post processing layer
                    rowsToFetch = reRankSize;
                }

                String fieldList = modifiableSolrParams.get(FIELD_LIST_PARAM);
                if(fieldList != null) {
                    modifiableSolrParams.set(FIELD_LIST_PARAM,fieldList+",score");
                }

                modifiableSolrParams.set(START, DEFAULT_START);
                modifiableSolrParams.set(ROWS, rowsToFetch);
                modifiableSolrParams.set(ORIGINAL_START, originalStart);
                modifiableSolrParams.set(ORIGINAL_ROWS, originalRows);
                modifiableSolrParams.set(DYNAMIC_SLICING_REQUIRED, Boolean.TRUE);
            }else {
                // if start is more than rerank size then, rerank is not shuffling original diversity.
                // Hence dynamic slicing not required
                modifiableSolrParams.set(DYNAMIC_SLICING_REQUIRED, Boolean.FALSE);

            }
        }
    }

    @Override
    public void process(ResponseBuilder rb) throws IOException {
    }

    @Override
    public String getDescription() {
        return "Pre processing component";
    }
}
