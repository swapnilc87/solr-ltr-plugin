package org.apache.solr.component;

public interface CustomDocument {
    String getBrand();
    Long getAddDate();
    Float getScore();
    Integer getStyleScoreSortField();
    String getGender();
    String getArticleType();
    String getMasterCategory();
    Integer getStyleId();
}
