package org.apache.solr.component;

import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.handler.component.SearchComponent;
import org.apache.solr.resource.ReRankingResource;
import org.apache.solr.response.ResultContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

public class PostProcessingComponent extends SearchComponent implements CharlesConstants {
    DiversityRanker diversityRanker = new DiversityRankerImpl();
    private static final Logger LOG = LoggerFactory.getLogger(PostProcessingComponent.class);
    private ReRankingResource reRankingResource = ReRankingResource.getInstance();

    @Override
    public void prepare(ResponseBuilder rb) throws IOException {
    }

    @Override
    public void process(ResponseBuilder rb) throws IOException {

        try {

            SolrParams solrParams = rb.req.getParams();
            ModifiableSolrParams params = solrParams instanceof ModifiableSolrParams ? (ModifiableSolrParams) solrParams : new ModifiableSolrParams(solrParams);
            NamedList values = rb.rsp.getValues();
            int responseIndex = values.indexOf(RESPONSE, 0);
            ResultContext resultContext = getResultContext(values, responseIndex);
            if (resultContext == null) {
                return;
            }

            Boolean dynamicSlicingRequired = params.getBool(DYNAMIC_SLICING_REQUIRED) == null ? Boolean.FALSE : params.getBool(DYNAMIC_SLICING_REQUIRED);
            if(LOG.isDebugEnabled()) {
                LOG.debug("In post processing component: " + dynamicSlicingRequired);
            }

            if (dynamicSlicingRequired) {
                List<CustomDocument> documents = getCharlesDocuments(resultContext);
                //Empty Results
                if (documents.size() == 0) {
                    if(LOG.isDebugEnabled()) {
                        LOG.debug("No documents found ; ");
                    }
                    return;
                }

                try {
                    long start = System.currentTimeMillis();
                    breakTies(documents);
                    documents = diversityRanker.diversifyResults(documents, reRankingResource.getReRankConfig());
                    long end = System.currentTimeMillis();
                    if(LOG.isDebugEnabled()) {
                        LOG.debug("Diversification of results done in ; {}",(end-start));
                    }
                }catch (Exception e) {
                    LOG.error("Error in diversifying results ", e);
                }
                SolrDocumentList solrDocumentList = sliceDocuments(params, resultContext, documents);
                values.setVal(responseIndex, solrDocumentList);
            }


        } catch (Exception e) {
            LOG.error("Error in post processing ", e);
        }
    }

    private void breakTies(List<CustomDocument> documents) {
        try {
            Collections.sort(documents, new Comparator<CustomDocument>() {
                @Override
                public int compare(CustomDocument d1, CustomDocument d2) {
                    int scoreComp = getSortOrder(d1.getScore(),d2.getScore());
                    if(scoreComp!=0) {
                        return scoreComp;
                    }
                    int styleScoreSort = getSortOrder(d1.getStyleScoreSortField(),d2.getStyleScoreSortField());
                    if(styleScoreSort!=0) {
                        return styleScoreSort;
                    }
                    int addDateSort = getSortOrder(d1.getAddDate(),d2.getAddDate());
                    if(addDateSort!=0) {
                        return addDateSort;
                    }

                    return getSortOrder(d1.getStyleId(),d2.getStyleId());

                }
            });
        } catch (Exception e) {
            LOG.error("Unable to break ties", e);
        }
    }

    public int getSortOrder(Comparable a,Comparable b) {
        if(a!=null ) {
            if(b!=null) {
                return -1*a.compareTo(b);
            }else {
                return -1;
            }

        }else if(b!=null) {
            return 1;
        }else {
            return 0;
        }
    }
    private ResultContext getResultContext(NamedList values, int responseIndex) {

        if (responseIndex != -1) {

            Object val = values.getVal(responseIndex);
            if (val instanceof ResultContext) {

                ResultContext resultContext = (ResultContext) val;
                return resultContext;
            }
        }
        return null;
    }

    /***
     * Function to convert the SolrDocuments into Charles Documents
     * @param resultContext
     * @return
     */
    private List<CustomDocument> getCharlesDocuments(ResultContext resultContext) {
        List<CustomDocument> documents = new ArrayList<>();

        Iterator<SolrDocument> documentsIterator = resultContext.getProcessedDocuments();
        if (documentsIterator != null) {
            while (documentsIterator.hasNext()) {
                SolrDocument doc = documentsIterator.next();
                CharlesDocument charlesDocument = new CharlesDocument(doc);
                documents.add(charlesDocument);
            }
        }

        return documents;
    }

    /***
     * This function takes a list of documents present as part of rerank layer and slices the list
     * based on start and rows params passed
     * @param params Solr Params
     * @param resultContext Result Context
     * @param documents List of charles documents
     * @return
     */
    private SolrDocumentList sliceDocuments(SolrParams params, ResultContext resultContext, List<CustomDocument> documents) {
        SolrDocumentList documentList = new SolrDocumentList();


        documentList.setNumFound(resultContext.getDocList().matches());
        documentList.setMaxScore(resultContext.getDocList().maxScore());

        int originalStart = params.getInt(ORIGINAL_START);
        int originalRows = params.getInt(ORIGINAL_ROWS);
        int end = originalStart + originalRows;

        // we expect more documents but matched documents are much lesser
        if (end > documents.size()) {
            end = documents.size();
        }
        if (originalStart > documents.size()) {
            return documentList;
        }

        //Slice the documents list based on start and end found
        for (int i = originalStart; i < end; i++) {
            CharlesDocument cDoc = (CharlesDocument) documents.get(i);
            documentList.add(cDoc.getSolrDocument());
        }

        return documentList;
    }

    @Override
    public String getDescription() {
        return "Post processing component";
    }


}
