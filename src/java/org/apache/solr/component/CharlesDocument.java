package org.apache.solr.component;

import org.apache.lucene.document.StoredField;
import org.apache.solr.common.SolrDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CharlesDocument implements CustomDocument {

    public static final String BRAND_FIELD = "global_attr_brand";
    public static final String START_DATE_FIELD = "add_date";
    public static final String SCORE = "score";
    public static final String STYLE_SCORE_SORT_FIELD = "style_store5_sort_field";
    public static final String GENDER_FIELD = "global_attr_gender";
    public static final String ARTICLE_TYPE_FIELD = "global_attr_article_type";
    public static final String MASTER_CATEGORY_FIELD = "global_attr_master_category";
    public static final String STYLEID = "styleid";
    private final Logger logger = LoggerFactory.getLogger(CharlesDocument.class);
    private SolrDocument solrDocument;

    public CharlesDocument(SolrDocument sdoc) {
        this.solrDocument = sdoc;
    }


    @Override
    public String getBrand() {
        return getStringField(BRAND_FIELD);
    }

    @Override
    public Long getAddDate() {
        try {
            if (solrDocument != null && solrDocument.get(START_DATE_FIELD) != null) {
                Object obj = solrDocument.get(START_DATE_FIELD);
                if (obj instanceof StoredField) {
                    StoredField field = (StoredField) obj;
                    return field.numericValue().longValue();
                }
            }
        } catch (Exception e) {
            logger.error("Error Getting start date");
        }
        return 0l;
    }

    @Override
    public Float getScore() {
        Float score = (Float) solrDocument.get("score");
        return score == null ? 0.0f : score;
    }

    @Override
    public Integer getStyleScoreSortField() {
        if (solrDocument != null && solrDocument.get(STYLE_SCORE_SORT_FIELD) != null) {
            Object obj = solrDocument.get(STYLE_SCORE_SORT_FIELD);
            if (obj instanceof StoredField) {
                StoredField field = (StoredField) obj;
                if(field.numericValue()!=null) {
                    return field.numericValue().intValue();
                }
            }
        }
        return 0;
    }

    @Override
    public String getGender() {
        return getStringField(GENDER_FIELD);
    }

    private String getStringField(String fieldName) {
        if (solrDocument != null && solrDocument.get(fieldName) != null) {
            Object obj = solrDocument.get(fieldName);
            if (obj instanceof StoredField) {
                StoredField field = (StoredField) obj;
                String ret = field.stringValue();
                if(ret!=null) {
                    return ret;
                }
            }
        }
        return "";
    }

    @Override
    public String getArticleType() {
        return getStringField(ARTICLE_TYPE_FIELD);
    }

    @Override
    public String getMasterCategory() {
        return getStringField(MASTER_CATEGORY_FIELD);
    }

    @Override
    public Integer getStyleId() {
        if (solrDocument != null && solrDocument.get(STYLEID) != null) {
            return ((StoredField) solrDocument.get(STYLEID)).numericValue().intValue();
        }
        return null;
    }

    public SolrDocument getSolrDocument() {
        return solrDocument;
    }
}
