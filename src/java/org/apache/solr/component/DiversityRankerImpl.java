package org.apache.solr.component;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DiversityRankerImpl implements DiversityRanker {

    public static final String IS_ACTIVE = "isActive";
    public static final String BRAND_CONSTRAINT = "brandConstraint";
    public static final String CONSTRAINT_VALUE = "constraintValue";
    public static final String NEW_BOOST = "newBoost";
    private static final Logger LOG = LoggerFactory.getLogger(DiversityRankerImpl.class);
    @Override
    public List<CustomDocument> diversifyResults(List<CustomDocument> documents, Map<String, Object> config) {
        if (documents == null || documents.size() == 0) {
            return documents;
        }
        List<CustomDocument> retDocs = new ArrayList<>(documents);

        // 1. Apply new Constraint
        boolean isNewBoost = true;
        int newBoostConstraintValue = 5;
        @SuppressWarnings("unchecked")
        Map<String, Object> newBoostConfig = (Map<String, Object>) config.get(NEW_BOOST);
        if (newBoostConfig != null) {
            try {
                isNewBoost = (boolean) newBoostConfig.get(IS_ACTIVE);
                newBoostConstraintValue = ((Long)newBoostConfig.get(CONSTRAINT_VALUE)).intValue();
            }catch (Exception e) {
                LOG.error(" Error in parsing config values:",e);
            }
        }

        if (isNewBoost) {
            retDocs = applyNewBoost(documents, newBoostConstraintValue);
        }

        // 2. Apply Brand Constraint
        boolean isBrandConstraint = true;
        int brandConstraintValue = 3;
        @SuppressWarnings("unchecked")
        Map<String, Object> brandBoostConfig = (Map<String, Object>) config.get(BRAND_CONSTRAINT);
        if (brandBoostConfig != null) {
            try {
                isBrandConstraint = (boolean) brandBoostConfig.get(IS_ACTIVE);
                brandConstraintValue = ((Long)brandBoostConfig.get(CONSTRAINT_VALUE)).intValue();
            }catch (Exception e) {
                LOG.error(" Error in parsing config values:",e);
            }
        }

        if (isBrandConstraint) {
            retDocs = applyBrandConstraint(retDocs, brandConstraintValue);
        }

        return retDocs;

    }

    private List<CustomDocument> applyBrandConstraint(List<CustomDocument> documents, int brandConstraintValue) {
        List<CustomDocument> retList = new ArrayList<>();
        List<CustomDocument> holdOutList = new ArrayList<>();
        List<CustomDocument> holdOutListCopy = new ArrayList<>();
        Queue<String> brandsToExclude = new CircularFifoQueue<String>(brandConstraintValue);
        int index = 0;
        while (index < documents.size()) {
            // first try hold out list
            holdOutListCopy = new ArrayList<>();
            for (int i = 0; i < holdOutList.size(); i++) {
                CustomDocument doc = holdOutList.get(i);
                String brand = doc.getBrand();
                if (canAddBrand(brand, brandsToExclude)) {
                    retList.add(doc);
                    if (brand != null) {
                        brandsToExclude.add(brand);
                    }
                } else {
                    holdOutListCopy.add(doc);
                }

            }
            holdOutList = holdOutListCopy;
            // now add the other docs
            CustomDocument doc = documents.get(index);
            String brand = doc.getBrand();
            if (canAddBrand(brand, brandsToExclude)) {
                retList.add(doc);
                if (brand != null) {
                    brandsToExclude.add(brand);
                }

            } else {
                holdOutList.add(doc);
            }

            index += 1;
        }
        // now add holdout list completely
        retList.addAll(holdOutList);
        return new ArrayList<>(retList);
    }

    private boolean canAddBrand(String brand, Queue<String> brandsToExclude) {
        if (brand != null && brandsToExclude.contains(brand)) {
            return false;
        }
        return true;
    }

    private List<CustomDocument> applyNewBoost(List<CustomDocument> documents, int constraintValue) {
        List<CustomDocument> retDocs = new ArrayList<>();
        Queue<CustomDocument> newDocs = new LinkedList<>();
        Queue<CustomDocument> oldDocs = new LinkedList<>();

        for (CustomDocument doc : documents) {
            if (doc.getAddDate() != null && getDaysElapsed(doc.getAddDate()) <= 15) {
                newDocs.add(doc);
            } else {
                oldDocs.add(doc);
            }
        }

        int count = 0;
        while (oldDocs.size() > 0 || newDocs.size() > 0) {
            CustomDocument doc = count % constraintValue == 0 ? newDocs.poll() : oldDocs.poll();
            count += 1;
            if (doc != null) {
                retDocs.add(doc);
            }
        }

        return retDocs;

    }

    private int getDaysElapsed(long timestamp) {
        return (int) ((System.currentTimeMillis() - timestamp) / (24 * 60 * 60 * 1000));
    }

}
