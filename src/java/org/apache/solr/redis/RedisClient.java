package org.apache.solr.redis;

import com.lambdaworks.redis.RedisURI;
import com.lambdaworks.redis.cluster.RedisClusterClient;
import com.lambdaworks.redis.cluster.api.StatefulRedisClusterConnection;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.util.ResourceLoader;
import org.apache.lucene.analysis.util.ResourceLoaderAware;
import org.apache.solr.common.SolrException;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.core.SolrResourceLoader;
import org.apache.solr.ltr.store.rest.ManagedRedisStore;
import org.apache.solr.rest.ManagedResource;
import org.apache.solr.rest.ManagedResourceObserver;
import org.apache.solr.util.plugin.NamedListInitializedPlugin;

import java.io.IOException;
import java.util.Map;

public class RedisClient implements ManagedResourceObserver, ResourceLoaderAware, NamedListInitializedPlugin {
    private ManagedRedisStore rd;
    private static RedisClient client = new RedisClient();
    private StatefulRedisClusterConnection connection;
    private RedisClient() {

    }

    public static synchronized RedisClient getInstance() {
        if(client == null) {
            client = new RedisClient();
        }

        return client;
    }

    public String getData(String key) {
        return (String) connection.sync().get(key);
    }


    public Map<String,String> hGetDataAsMap(String key) {
        return  (Map<String,String>) connection.sync().hgetall(key);
    }

    @Override
    public void onManagedResourceInitialized(NamedList<?> args, ManagedResource res) throws SolrException {
        if (res instanceof ManagedRedisStore) {
            rd = (ManagedRedisStore)res;
            try {
                if(connection!=null) {
                    connection.close();
                }
                connection = masterConn(createRedisClusterClient());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void inform(ResourceLoader loader) throws IOException {
        final SolrResourceLoader solrResourceLoader = (SolrResourceLoader) loader;
        ManagedRedisStore.registerManagedRedisStore(solrResourceLoader, this);

    }

    public ManagedRedisStore getRd() {
        return rd;
    }

    public void setRd(ManagedRedisStore rd) {
        this.rd = rd;
    }

    @Override
    public void init(NamedList args) {
    }

    private StatefulRedisClusterConnection<String, String> masterConn(RedisClusterClient clusterClient){
        return clusterClient.connect(SnappyCompressor.wrap(new StringCodec()));
    }
    private synchronized RedisClusterClient createRedisClusterClient() throws IOException {
        if (StringUtils.isEmpty(rd.getHost())) {
            throw new IOException("Could not setup redis:cluster: could not find the key redis.cluster.host.");
        }
        RedisURI clusterUri = new RedisURI();
        clusterUri.setHost(rd.getHost());
        clusterUri.setPort(rd.getPort());
        clusterUri.setTimeout(rd.getConnectionTimeout());
        RedisClusterClient clusterClient = RedisClusterClient.create(clusterUri);
        clusterClient.setDefaultTimeout(rd.getReadTimeout(),rd.READ_TIMEOUT_UNIT);
        return clusterClient;
    }
}
