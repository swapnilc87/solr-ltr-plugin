package org.apache.solr.ltr.store.rest;

import org.apache.solr.common.SolrException;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.core.SolrResourceLoader;
import org.apache.solr.rest.BaseSolrResource;
import org.apache.solr.rest.ManagedResource;
import org.apache.solr.rest.ManagedResourceObserver;
import org.apache.solr.rest.ManagedResourceStorage;

import java.util.HashMap;
import java.util.Map;

public class ManagedPostProcessingStore extends ManagedResource implements ManagedResource.ChildResourceSupport {

    Map<String,Object> configData;

    public static final String REST_END_POINT = "/schema/post-processing-store";

    public ManagedPostProcessingStore(String resourceId, SolrResourceLoader loader, ManagedResourceStorage.StorageIO storageIO) throws SolrException {
        super(resourceId, loader, storageIO);
        configData = new HashMap<>();
    }

    public static void registerManagedPostProcessingStore(SolrResourceLoader solrResourceLoader, ManagedResourceObserver managedResourceObserver) {
        solrResourceLoader.getManagedResourceRegistry().registerManagedResource(
                REST_END_POINT,
                ManagedPostProcessingStore.class,
                managedResourceObserver);
    }

    @Override
    protected void onManagedDataLoadedFromStorage(NamedList<?> managedInitArgs, Object updates) throws SolrException {
        if(updates instanceof Map) {
            Map<String,Object> map = (Map<String,Object>) updates;
            this.configData = new HashMap<>(map);
        }
    }

    @Override
    protected Object applyUpdatesToManagedData(Object updates) {
        if(updates instanceof Map) {
            Map<String,Object> map = (Map<String,Object>) updates;
            if(map!=null) {
                this.configData = new HashMap<>(map);
            }
        }
        return updates;
    }

    @Override
    public void doDeleteChild(BaseSolrResource endpoint, String childId) {
        this.configData.clear();
    }

    @Override
    public void doGet(BaseSolrResource endpoint, String childId) {
    }

    public Map<String,Object> getConfigData() {
        return configData == null  ? new HashMap<>():new HashMap(configData);
    }
}
