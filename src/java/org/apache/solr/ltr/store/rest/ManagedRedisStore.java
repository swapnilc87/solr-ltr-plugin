package org.apache.solr.ltr.store.rest;

import org.apache.solr.common.SolrException;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.core.SolrResourceLoader;
import org.apache.solr.redis.RedisClient;
import org.apache.solr.rest.BaseSolrResource;
import org.apache.solr.rest.ManagedResource;
import org.apache.solr.rest.ManagedResourceObserver;
import org.apache.solr.rest.ManagedResourceStorage;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class ManagedRedisStore extends ManagedResource implements ManagedResource.ChildResourceSupport{

    public static final String REDIS_CLUSTER_HOST = "redis.cluster.host";
    public static final String REDIS_CLUSTER_PORT = "redis.cluster.port";
    public static final String REDIS_CONNECTION_TIMEOUT = "redis.connection.timeout";
    public static final String REDIS_READ_TIMEOUT = "redis.read.timeout";
    private Object managedData;
    List<Map<String,Object>> configData;
    /** the model store rest endpoint **/
    public static final String REST_END_POINT = "/schema/redis-store";

    private String host;
    private Integer port;
    private Long connectionTimeout=1000l;
    private Long readTimeout=100l;
    public static final TimeUnit CONNECTION_TIMEOUT_UNIT = TimeUnit.MILLISECONDS;
    public static final TimeUnit READ_TIMEOUT_UNIT = TimeUnit.MILLISECONDS;


    /**
     * Initializes this managed resource, including setting up JSON-based storage using
     * the provided storageIO implementation, such as ZK.
     */
    public ManagedRedisStore(String resourceId, SolrResourceLoader loader, ManagedResourceStorage.StorageIO storageIO) throws SolrException {
        super(resourceId, loader, storageIO);
        configData = new ArrayList<>();
    }

    public static void registerManagedRedisStore(SolrResourceLoader solrResourceLoader, ManagedResourceObserver managedResourceObserver) {
        solrResourceLoader.getManagedResourceRegistry().registerManagedResource(
                REST_END_POINT,
                ManagedRedisStore.class,
                managedResourceObserver);
    }

    @Override
    protected void onManagedDataLoadedFromStorage(NamedList<?> managedInitArgs, Object managedData) throws SolrException {
        this.managedData = managedData;
        if (managedData instanceof List) {
            final List<Map<String,Object>> map = (List<Map<String,Object>>) managedData;
            addRedisConfigFromList(map);
        }
    }

    @Override
    protected Object applyUpdatesToManagedData(Object updates) {

        final List<Object> list = new ArrayList<>(1);
        if (updates instanceof List) {
            final List<Map<String,Object>> map = (List<Map<String,Object>>) updates;
            addRedisConfigFromList(map);
            return updates;
        }

        return list;
    }

    private void addRedisConfigFromList(List<Map<String,Object>> list) {
        configData.clear();
        configData.addAll(list);
        for (Map<String,Object> map:list) {
            if (map.containsKey(REDIS_CLUSTER_HOST)) {
                this.host = (String) map.get(REDIS_CLUSTER_HOST);
            }
            if (map.containsKey(REDIS_CLUSTER_PORT)) {
                this.port = ((Long) map.get(REDIS_CLUSTER_PORT)).intValue();
            }
            if (map.containsKey(REDIS_CONNECTION_TIMEOUT)) {
                this.connectionTimeout = (Long) map.get(REDIS_CONNECTION_TIMEOUT);
            }
            if (map.containsKey(REDIS_READ_TIMEOUT)) {
                this.readTimeout = (Long) map.get(REDIS_READ_TIMEOUT);
            }

        }
        RedisClient.getInstance().onManagedResourceInitialized(null,this);
    }

    @Override
    public void doDeleteChild(BaseSolrResource endpoint, String childId) {

    }

    @Override
    public void doGet(BaseSolrResource endpoint, String childId) {
    }

    public Object getManagedData() {
        return managedData;
    }

    public void setManagedData(Object managedData) {
        this.managedData = managedData;
    }

    public List<Map<String, Object>> getConfigData() {
        return new ArrayList<>(configData);
    }

    public String getHost() {
        return host;
    }

    public Integer getPort() {
        return port;
    }

    public Long getConnectionTimeout() {
        return connectionTimeout;
    }

    public Long getReadTimeout() {
        return readTimeout;
    }

}
