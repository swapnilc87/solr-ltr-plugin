package org.apache.solr.ltr.feature;

import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.solr.request.SolrQueryRequest;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class BooleanFeature extends Feature  {

    private float configValue = 0f;
    private float defaultValue = 0f;
    private String configValueStr = null;

    private Object value = null;
    private Boolean required = null;

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
        if(value!=null) {
            if(value instanceof String) {
                this.configValueStr = (String) value;
            }else {
                this.configValue = 0f;
            }
        }
    }

    public boolean isRequired() {
        return Boolean.TRUE.equals(required);
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    @Override
    public LinkedHashMap<String,Object> paramsToMap() {
        final LinkedHashMap<String,Object> params = new LinkedHashMap<>(2, 1.0f);
        params.put("value", value);
        if (required != null) {
            params.put("required", required);
        }
        return params;
    }

    @Override
    protected void validate() throws FeatureException {

    }

    public BooleanFeature(String name, Map<String,Object> params) {
        super(name, params);
    }

    @Override
    public FeatureWeight createWeight(IndexSearcher searcher, boolean needsScores,
                                      SolrQueryRequest request, Query originalQuery, Map<String,String[]> efi)
            throws IOException {
        return new BooleanFeature.BooleanFeatureWeight(searcher, request, originalQuery, efi);
    }

    @Override
    public float getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(float defaultValue) {
        this.defaultValue = defaultValue;
    }

    public class BooleanFeatureWeight extends FeatureWeight {

        final protected Float featureValue;

        public BooleanFeatureWeight(IndexSearcher searcher,
                                  SolrQueryRequest request, Query originalQuery, Map<String,String[]> efi) {
            super(BooleanFeature.this, searcher, request, originalQuery, efi);
            if (configValueStr != null) {
                final String expandedValue = macroExpander.expand(configValueStr);
                if (expandedValue != null && !"".equalsIgnoreCase(expandedValue.trim())) {
                    featureValue = 1.0f;
                } else {
                    featureValue = configValue;
                }
            } else {
                    featureValue = configValue;
            }
        }

        @Override
        public FeatureScorer scorer(LeafReaderContext context) throws IOException {
            if(featureValue!=null) {
                return new ValueFeatureScorer(this, featureValue,
                        DocIdSetIterator.all(DocIdSetIterator.NO_MORE_DOCS));
            } else {
                return null;
            }
        }





    }
}
