package org.apache.solr.ltr.feature;

import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.solr.request.SolrQueryRequest;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

public class QDFeature extends Feature{
    String featureName;
    Set<String> fieldsToLoad = new HashSet<>();
    public static final String ID_FIELD = "styleid";
    Gson gson = new Gson();

    Logger LOG = LoggerFactory.getLogger(QDFeature.class);
    public QDFeature(String name, Map<String, Object> params) {
        super(name, params);
        fieldsToLoad.add(ID_FIELD);
    }

    @Override
    protected void validate() throws FeatureException {
        //TODO add any checks if required
    }

    @Override
    public FeatureWeight createWeight(IndexSearcher searcher, boolean needsScores, SolrQueryRequest request, Query originalQuery, Map<String, String[]> efi) throws IOException {
        return new QDFeatureWeight(searcher,request,originalQuery,efi);
    }

    @Override
    public LinkedHashMap<String, Object> paramsToMap() {
        //TODO to be filled correctly
        final LinkedHashMap<String,Object> params = new LinkedHashMap<>(1, 1.0f);
        params.put("featureName",featureName);
        return params;
    }

    public class QDFeatureWeight extends FeatureWeight{
        Map<String,Double> documentScores = new HashMap<>();

        /**
         * Initialize a feature without the normalizer from the feature file. This is
         * called on initial construction since multiple models share the same
         * features, but have different normalizers. A concrete model's feature is
         * copied through featForNewModel().
         *
         * @param searcher
         * @param request
         * @param originalQuery
         * @param efi
         */
        public QDFeatureWeight(IndexSearcher searcher, SolrQueryRequest request, Query originalQuery, Map<String, String[]> efi) {
            super(QDFeature.this, searcher, request, originalQuery, efi);
            String[] strNames = efi.get(name);
            if(LOG.isDebugEnabled()) {
                LOG.debug("In QDFeature ; for feature {} and query {}",name,request.getOriginalParams().get("query_str"));
            }
            if(strNames!=null && strNames.length>0 &&strNames[0]!=null) {
                long start = 0l;
                if(LOG.isDebugEnabled()) {
                    start = System.currentTimeMillis();
                }
                loadMapFromJson(strNames[0]);
                if(LOG.isDebugEnabled()) {
                    long end = System.currentTimeMillis();
                    LOG.debug("Deserialized Feature json in ; {} for feature {} and query {}",(end-start),name,request.getOriginalParams().get("query_str"));
                }
            }

        }

        private void loadMapFromJson(String strName) {
            try {
                Type type = new TypeToken<Map<String, Double>>(){}.getType();
                Map<String, Double> myMap = gson.fromJson(strName, type);
                if(myMap!=null && !myMap.isEmpty()) {
                    documentScores.putAll(myMap);
                }

            }catch (Exception e) {
               LOG.error("Features for {} not found ; data= {}",name,strName);
            }

        }

        @Override
        public FeatureScorer scorer(LeafReaderContext context) throws IOException {
            return new QDFeatureScorer(QDFeatureWeight.this,context,DocIdSetIterator.all(DocIdSetIterator.NO_MORE_DOCS));
        }


        public class QDFeatureScorer extends FeatureScorer {
            LeafReaderContext context = null;

            public QDFeatureScorer(FeatureWeight weight, LeafReaderContext context, DocIdSetIterator itr) {
                super(weight, itr);
                this.context = context;
            }

            @Override
            public float score() throws IOException {
                final Document document = context.reader().document(itr.docID(),
                        fieldsToLoad);
                String idStr = document.get(ID_FIELD);

                if(StringUtils.isNotEmpty(idStr) && documentScores.containsKey(idStr)) {
                    return documentScores.get(idStr).floatValue();
                }
                return 0;
            }
        }
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }
}


