package org.apache.solr.ltr.feature;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Scorer;
import org.apache.solr.request.SolrQueryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;

public class QDIndirectSolrFeature extends SolrFeature{
    String featureName;
    Set<String> fieldsToLoad = new HashSet<>();
    public static final String ID_FIELD = "styleid";
    Gson gson = new Gson();

    Logger LOG = LoggerFactory.getLogger(QDIndirectSolrFeature.class);
    public QDIndirectSolrFeature(String name, Map<String, Object> params) {
        super(name, params);
        fieldsToLoad.add(ID_FIELD);
    }

    @Override
    protected void validate() throws FeatureException {
        //TODO add any checks if required
    }

    @Override
    public FeatureWeight createWeight(IndexSearcher searcher, boolean needsScores, SolrQueryRequest request, Query originalQuery, Map<String, String[]> efi) throws IOException {
        return new QDSolrFeatureWeight(searcher,request,originalQuery,efi);
    }

    @Override
    public LinkedHashMap<String, Object> paramsToMap() {
        //TODO to be filled correctly

        final LinkedHashMap<String,Object> params = new LinkedHashMap<>(1, 1.0f);
        params.put("featureName",featureName);
        params.putAll(super.paramsToMap());
        return params;
    }

    public class QDSolrFeatureWeight extends SolrFeatureWeight{
        Map<String,Double> documentScores = new HashMap<>();

        /**
         * Initialize a feature without the normalizer from the feature file. This is
         * called on initial construction since multiple models share the same
         * features, but have different normalizers. A concrete model's feature is
         * copied through featForNewModel().
         *
         * @param searcher
         * @param request
         * @param originalQuery
         * @param efi
         */
        public QDSolrFeatureWeight(IndexSearcher searcher,
                                   SolrQueryRequest request, Query originalQuery, Map<String,String[]> efi) throws IOException{
            super (searcher, request, originalQuery, efi);
            String[] strNames = efi.get(name);
            if(LOG.isDebugEnabled()) {
                LOG.debug("In QDSolrFeature ; for feature {} and query {}",name,request.getOriginalParams().get("query_str"));
            }
            if(strNames!=null && strNames.length>0 &&strNames[0]!=null) {
                long start = 0l;
                if(LOG.isDebugEnabled()) {
                    start = System.currentTimeMillis();
                }
                loadMapFromJson(strNames[0]);
                if(LOG.isDebugEnabled()) {
                    long end = System.currentTimeMillis();
                    LOG.debug("Deserialized Feature json in ; {} for feature {} and query {}",(end-start),name,request.getOriginalParams().get("query_str"));
                }
            }

        }

        private void loadMapFromJson(String strName) {
            try {
                Type type = new TypeToken<Map<String, Double>>(){}.getType();
                Map<String, Double> myMap = gson.fromJson(strName, type);
                if(myMap!=null && !myMap.isEmpty()) {
                    documentScores.putAll(myMap);
                }

            }catch (Exception e) {
                LOG.error("Features for {} not found ; data= {}",name,strName);
            }

        }

        @Override
        public FeatureScorer scorer(LeafReaderContext context) throws IOException {

            FeatureScorer scorer = super.scorer(context);
            return new QDSolrFeatureScorer(this,scorer,(SolrFeatureScorerIterator) scorer.iterator());


        }


        public class QDSolrFeatureScorer extends SolrFeatureScorer {
            public QDSolrFeatureScorer(FeatureWeight weight, Scorer solrScorer,
                                       SolrFeatureScorerIterator itr) {
                super(weight, solrScorer,itr);
            }

            @Override
            public float score() throws IOException {
                String idStr = ""+ (int)super.score();


                if(StringUtils.isNotEmpty(idStr) && documentScores.containsKey(idStr)) {
                    return documentScores.get(idStr).floatValue();
                }
                return 0;
            }
        }
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }
}


