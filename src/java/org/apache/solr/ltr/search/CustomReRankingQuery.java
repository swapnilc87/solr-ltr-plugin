package org.apache.solr.ltr.search;

import java.io.IOException;

import org.apache.lucene.index.*;
import org.apache.lucene.queries.CustomScoreProvider;
import org.apache.lucene.queries.CustomScoreQuery;
import org.apache.lucene.search.Query;

public class CustomReRankingQuery extends CustomScoreQuery {

	public CustomReRankingQuery(Query subQuery) {
		super(subQuery);
	}
	
	
	public class CustomReRankingQueryScoreProvider extends CustomScoreProvider {

		String _field;
		
		public CustomReRankingQueryScoreProvider(String field, LeafReaderContext context) {
			super(context);
			_field = field;
		}
	
		// Rescores by counting the number of terms in the field
		public float customScore(int doc, float subQueryScore, float valSrcScores[]) throws IOException {
			IndexReader r = context.reader();
			Terms tv = r.getTermVector(doc, _field);
			TermsEnum termsEnum = null;
			termsEnum = tv.iterator();
		    int numTerms = 0;
			while((termsEnum.next()) != null) {
		    	numTerms++;
		    }
			return (float)(numTerms);
		}

	
	}
	
	protected CustomScoreProvider getCustomScoreProvider(
			LeafReaderContext context) throws IOException {
		return new CustomReRankingQueryScoreProvider("tag", context);
	}
	
	

}