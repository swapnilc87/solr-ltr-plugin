package org.apache.solr.ltr.search;

import org.apache.lucene.analysis.util.ResourceLoader;
import org.apache.lucene.search.Query;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.core.SolrResourceLoader;
import org.apache.solr.ltr.store.rest.ManagedPostProcessingStore;
import org.apache.solr.ltr.store.rest.ManagedRedisStore;
import org.apache.solr.redis.RedisClient;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.resource.ReRankingResource;
import org.apache.solr.search.QParser;
import org.apache.solr.search.SyntaxError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

public class ExternalLTRQParserPlugin extends LTRQParserPlugin {
    private RedisClient redisClient = RedisClient.getInstance();
    private static final String PREFIX = "ltr_";
    Logger LOG = LoggerFactory.getLogger(ExternalLTRQParserPlugin.class);

    @Override
    public QParser createParser(String qstr, SolrParams localParams,
                                SolrParams params, SolrQueryRequest req) {
        return new ExternalLTRQParser(qstr, localParams, params, req);
    }

    @Override
    public void inform(ResourceLoader loader) throws IOException {
        final SolrResourceLoader solrResourceLoader = (SolrResourceLoader) loader;
        ManagedRedisStore.registerManagedRedisStore(solrResourceLoader, redisClient);
        ManagedPostProcessingStore.registerManagedPostProcessingStore(solrResourceLoader,ReRankingResource.getInstance());
        super.inform(loader);
    }

    public class ExternalLTRQParser extends LTRQParser {

        public ExternalLTRQParser(String qstr, SolrParams localParams, SolrParams params, SolrQueryRequest req) {
            super(qstr, localParams, params, req);
        }

        @Override
        public Query parse() throws SyntaxError {

            try {
                long start = 0l;
                if(LOG.isDebugEnabled()) {
                    start = System.currentTimeMillis();
                }
                Map<String,String> featureValueMap = redisClient.hGetDataAsMap(PREFIX+params.get("query_str"));

                ModifiableSolrParams modifiableSolrParams = new ModifiableSolrParams(localParams);
                if (featureValueMap != null) {
                    if(LOG.isDebugEnabled()) {
                        long redisEnd = System.currentTimeMillis();
                        LOG.debug("found data for key in redis: {} in time {}", PREFIX + params.get("query_str"),(redisEnd-start));
                    }
                    for (String featureName : featureValueMap.keySet()) {
                        modifiableSolrParams.add(EXTERNAL_FEATURE_INFO + featureName, featureValueMap.get(featureName));
                    }
                }else {
                    if(LOG.isDebugEnabled()) {
                        LOG.debug("No data found for key in redis; " + PREFIX + params.get("query_str"));
                    }
                }
                localParams = modifiableSolrParams;
            } catch (Exception e) {
                LOG.error("got exception while parsing json data from redis for query - "+params.get("query_str"),e);
            }
            return super.parse();
        }
    }

}
