package org.apache.solr.ltr.response.transform;

import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.redis.RedisClient;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.response.transform.DocTransformer;

import java.util.Map;

public class ExternalLTRFeatureLoggerTransformerFactory extends LTRFeatureLoggerTransformerFactory {
    static final String EXTERNAL_FEATURE_INFO = "efi.";
    private static final String PREFIX = "ltr_";

    @Override
    public DocTransformer create(String name, SolrParams localParams, SolrQueryRequest req) {

        ModifiableSolrParams modifiableSolrParams = new ModifiableSolrParams(localParams);
        try {
            Map<String,String> featureValueMap = RedisClient.getInstance().hGetDataAsMap(PREFIX+req.getParams().get("query_str"));
            if (featureValueMap != null) {
                for (String featureName : featureValueMap.keySet()) {
                    modifiableSolrParams.add(EXTERNAL_FEATURE_INFO + featureName, featureValueMap.get(featureName));
                }
            }
        } catch (Exception e) {
//                e.printStackTrace();
            System.err.println("got exception while parsing json data from redis" + e);
        }
        return super.create(name, modifiableSolrParams, req);
    }
}
