package org.apache.solr.custom;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.queries.CustomScoreProvider;
import org.apache.lucene.queries.CustomScoreQuery;
import org.apache.lucene.search.Query;

import java.io.IOException;
import java.util.List;

public class MyQuery extends CustomScoreQuery {

  public MyQuery(Query subQuery) {
    super(subQuery);
  }

  @Override
  protected CustomScoreProvider getCustomScoreProvider(
          LeafReaderContext context) throws IOException {
    System.out.println("initializing myquery");
    System.out.println(super.toString());
    return new MyScoreProvider(context);
  }

  class MyScoreProvider extends CustomScoreProvider {

    public MyScoreProvider(LeafReaderContext context) {
      super(context);
    }

    @Override
    public float customScore(int doc, float subQueryScore,
            float valSrcScore) throws IOException {
      return customScore(doc, subQueryScore, new float[]{valSrcScore});
    }

    @Override
    public float customScore(int doc, float subQueryScore,
            float[] valSrcScores) throws IOException {
      // Method is called for every 
      // matching document of the subQuery

      Document d = context.reader().document(doc);
      // plugin external score calculation based on the fields...
      List fields = d.getFields();
      // and return the custom score
      float score = 1.0f;
      String count_options_availble = d.get("count_options_availbale");
      String personalizationScoreStr = d.get("myVal");
      if(personalizationScoreStr==null) {
        personalizationScoreStr="0";
      }
      float personalizationScore = Float.valueOf(personalizationScoreStr);
      float count_options_availbale_flt = Float.valueOf(count_options_availble);
      System.out.println("scoring in custom scorer:"+doc+" :: "+getTotalScore(valSrcScores)+"::"+count_options_availbale_flt+"::"+personalizationScore+"::"+fields);
      System.out.println(super.toString());


      return subQueryScore+getTotalScore(valSrcScores)+count_options_availbale_flt;

    }

    private float getTotalScore(float scores[]) {
      if(scores==null || scores.length==0) {
        return 0.0f;
      }
      float total = 0;
      for(float f:scores) {
        total+=f;
      }
      return total;
    }
  }
}